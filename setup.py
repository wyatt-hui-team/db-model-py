from setuptools import find_packages, setup

setup(
    name='db_model_py',
    version='1.7.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'mysqlclient~=1.4',
        'sqlalchemy~=1.3'
    ]
)
