### Dependencies
* Python 3.x
* pip3

### Dependencies
* sudo apt-get install libmysqlclient-dev

### Installation
Require the repository to your project
* /setup.py

    Add the following to 'install_requires' (and replace the {version})

    ```
    'db-model-py @ git+ssh://git@git.cloudbreakr.com/developer/db-model-py.git@{version}#egg=db-model-py'
    ```

    Use pip to install again

    * (Optional) Use `-e` option to edit this repository

Init database engine with configuration

```
from db_model_py.declarative.some import SomeDeclarative

SomeDeclarative.set_engine_to_base({
    'username': '{username}',
    'password': '{password}',
    'host': '{host}',
    'database': '{database}',
    'param': {}
})
```

Import and use function in repository

```
from db_model_py.repositories.some.some_repository import SomeRepository

SomeRepository.get_somebody()
```

Close database engine

```
SomeDeclarative.close()
```

### Development Installation
Create an environment
* /

    ```
    python3 -m venv venv
    ```

Activate the environment

* /

    Before you work on your project, activate the corresponding environment
    ```
    . venv/bin/activate
    ```

    Upgrade pip and setuptools
    ```
    pip3 install --upgrade pip
    pip3 install --upgrade setuptools
    ```

Installation for Python packages
* /

    Copy `.env.example` to `.env`
    ```
    pip3 install -e .
    ```

### Deployment instructions
For version update, please edit the version number in
* setup.py

### Team contacts
* Wyatt Hui (wyatt@cloudbreakr.com)

### License
Cloudbreakr
