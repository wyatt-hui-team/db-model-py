from sqlalchemy import Column, ForeignKey, Integer
from ..common.interest_classification import declarate as declarate_base

def declarate(base):
    class FBUserInterestClassification(declarate_base(base)):
        __tablename__ = 'fb_user_interest_classification'

        fb_user_id = Column(Integer, ForeignKey('fb_user.id'), primary_key=True)

    return FBUserInterestClassification
