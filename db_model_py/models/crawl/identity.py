from ..common.identity import declarate as declarate_base

def declarate(base):
    class Identity(declarate_base(base)):
        pass

    return Identity
