from sqlalchemy import Column, ForeignKey, Integer, String
from ..common.interest_classification import declarate as declarate_base

def declarate(base):
    class IGPostInterestClassification(declarate_base(base)):
        __tablename__ = 'ig_post_interest_classification'

        ig_post_id = Column(String(30), ForeignKey('ig_post.ig_post_id'), primary_key=True)

    return IGPostInterestClassification
