from sqlalchemy import Column, ForeignKey, String
from ..common.interest_classification import declarate as declarate_base

def declarate(base):
    class FBPostInterestClassification(declarate_base(base)):
        __tablename__ = 'fb_post_interest_classification'

        fb_post_id = Column(String(40), ForeignKey('fb_post.fb_post_id'), primary_key=True)

    return FBPostInterestClassification
