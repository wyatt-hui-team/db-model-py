from ..common.ig_post import declarate as declarate_base

def declarate(base):
    class IGPost(declarate_base(base)):
        pass

    return IGPost
