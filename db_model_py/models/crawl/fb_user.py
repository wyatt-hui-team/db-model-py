from sqlalchemy import Column, Float, Integer, String, Text
from ..common.fb_user import declarate as declarate_base

def declarate(base):
    class FBUser(declarate_base(base)):
        personalInterest = Column(String(255))
        personalInfo = Column(Text())
        engagement = Column(Float)
        explosivenessPostId = Column(String(30))
        explosivenessPostLike = Column(Integer)
        explosivenessPostComment = Column(Integer)
        explosivenessPostShare = Column(Integer)
        postCount97 = Column(Integer)
        likeCount97 = Column(Integer)
        commentCount97 = Column(Integer)
        shareCount97 = Column(Integer)
        postCount90 = Column(Integer)
        likeCount90 = Column(Integer)
        commentCount90 = Column(Integer)
        shareCount90 = Column(Integer)
        postCount7 = Column(Integer)
        likeCount7 = Column(Integer)
        commentCount7 = Column(Integer)
        shareCount7 = Column(Integer)
        postCount30 = Column(Integer)
        likeCount30 = Column(Integer)
        commentCount30 = Column(Integer)
        shareCount30 = Column(Integer)

    return FBUser
