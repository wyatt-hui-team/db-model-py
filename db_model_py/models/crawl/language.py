from ..common.language import declarate as declarate_base

def declarate(base):
    class Language(declarate_base(base)):
        pass

    return Language
