from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from ..common.inf_power import declarate as declarate_base

def declarate(base):
    class IGInfPower(declarate_base(base)):
        __tablename__ = 'ig_inf_power'

        infId = Column(Integer, ForeignKey('influencer.id'), primary_key=True)

        influencer = relationship('Influencer')

    return IGInfPower
