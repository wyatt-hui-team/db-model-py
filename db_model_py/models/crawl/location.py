from ..common.location import declarate as declarate_base

def declarate(base):
    class Location(declarate_base(base)):
        pass

    return Location
