from sqlalchemy import Column, Integer, String
from ..common.influencer import declarate as declarate_base

def declarate(base):
    class Influencer(declarate_base(base)):
        user_id = Column(Integer)
        sourceFrom = Column(String(255))

    return Influencer
