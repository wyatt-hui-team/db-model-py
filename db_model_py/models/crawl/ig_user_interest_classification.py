from sqlalchemy import Column, ForeignKey, Integer
from ..common.interest_classification import declarate as declarate_base

def declarate(base):
    class IGUserInterestClassification(declarate_base(base)):
        __tablename__ = 'ig_user_interest_classification'

        ig_user_id = Column(Integer, ForeignKey('ig_user.id'), primary_key=True)

    return IGUserInterestClassification
