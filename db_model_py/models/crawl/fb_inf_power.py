from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from ..common.inf_power import declarate as declarate_base

def declarate(base):
    class FBInfPower(declarate_base(base)):
        __tablename__ = 'fb_inf_power'

        infId = Column(Integer, ForeignKey('influencer.id'), primary_key=True)

        influencer = relationship('Influencer')

    return FBInfPower
