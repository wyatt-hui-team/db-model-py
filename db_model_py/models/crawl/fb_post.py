from ..common.fb_post import declarate as declarate_base

def declarate(base):
    class FBPost(declarate_base(base)):
        pass

    return FBPost
