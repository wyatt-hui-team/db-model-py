from sqlalchemy import Column, Integer, SmallInteger, String
from ..common.ig_user import declarate as declarate_base

def declarate(base):
    class IGUser(declarate_base(base)):
        ig_business_id = Column(Integer)
        token = Column(String(255))
        recent7PostCount = Column(Integer)
        recent7CommentCount = Column(Integer)
        recent7LikeCount = Column(Integer)
        recent97PostCount = Column(Integer)
        recent97CommentCount = Column(Integer)
        recent97LikeCount = Column(Integer)
        explosivenessPostId = Column(String(30))
        explosivenessPostLike = Column(Integer)
        explosivenessPostComment = Column(Integer)
        explosivenessPostShare = Column(Integer)
        postCount97 = Column(Integer)
        likeCount97 = Column(Integer)
        commentCount97 = Column(Integer)
        postCount90 = Column(Integer)
        likeCount90 = Column(Integer)
        commentCount90 = Column(Integer)
        postCount7 = Column(Integer)
        likeCount7 = Column(Integer)
        commentCount7 = Column(Integer)
        postCount30 = Column(Integer)
        likeCount30 = Column(Integer)
        commentCount30 = Column(Integer)
        photoUpdatedAt = Column(Integer)
        crawlPhotoStatus = Column(SmallInteger)
        dynamodbPostCount = Column(Integer)

    return IGUser
