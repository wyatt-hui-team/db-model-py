from sqlalchemy import Column, Float, Integer, SmallInteger, String
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Table
from ..model import Model

def declarate(base):
    class TestInfluencer(base, Model):
        __tablename__ = 'test_influencer'

        user_id = Column(String(64), primary_key=True)
        followers = Column(Integer)
        following = Column(Integer)
        num_posts = Column(Integer)
        has_story = Column(SmallInteger)
        fwing_fwers = Column(Float)
        fwing_post = Column(Float)
        fwers_post = Column(Float)
        has_digit = Column(SmallInteger)
        has_digit_end = Column(SmallInteger)
        numeric_alpha_ratio = Column(Float)
        username = Column(String(100))

        followers = relationship(
            'TestFollower',
            secondary=Table(
                'test_followed_by',
                base.metadata,
                extend_existing=True
            )
        )

    return TestInfluencer
