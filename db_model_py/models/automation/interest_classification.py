from sqlalchemy import Column, Integer, String
from ..model import Model

def declarate(base):
    class InterestClassification(base, Model):
        __tablename__ = 'interest_classification'

        id = Column(Integer, primary_key=True)
        name = Column(String(100))
        name_zh = Column(String(100))
        parent_id = Column(Integer)

    return InterestClassification
