from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class TestFollowedBy(base, Model):
        __tablename__ = 'test_followed_by'

        influencer_id = Column(Integer, ForeignKey('test_influencer.user_id'), primary_key=True)
        follower_id = Column(Integer, ForeignKey('test_followers.user_id'), primary_key=True)

        follower = relationship('TestFollower')
        influencer = relationship('TestInfluencer')

    return TestFollowedBy
