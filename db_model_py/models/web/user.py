from sqlalchemy import BigInteger, Column, ForeignKey, Integer, SmallInteger, String
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class User(base, Model):
        __tablename__ = 'user'

        id = Column(Integer, primary_key=True)
        type = Column(String(45))
        email = Column(String(255))
        password = Column(String(100))
        name = Column(String(45))
        lastName = Column(String(45))
        phone = Column(String(20))
        gender = Column(SmallInteger)
        verifyCode = Column(String(255))
        passwordResetCode = Column(String(255))
        status = Column(String(45))
        lastLoginDate = Column(Integer)
        createdAt = Column(Integer)
        updatedAt = Column(Integer)
        businessId = Column(Integer, ForeignKey('business_user.id'))
        ip = Column(String(45))
        locationId = Column(Integer, ForeignKey('location.id'))
        promptUserType = Column(BigInteger)
        languageId = Column(Integer, ForeignKey('language.id'))
        fb_login_id = Column(BigInteger)
        isPremiumRegister = Column(SmallInteger)
        remember_token = Column(String(255))
        lastOnline = Column(Integer)

        business_user = relationship('BusinessUser')
        location = relationship('Location')
        language = relationship('Language')
        influencer = relationship('Influencer')

    return User
