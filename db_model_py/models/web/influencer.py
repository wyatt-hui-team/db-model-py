from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from ..common.influencer import declarate as declarate_base

def declarate(base):
    class Influencer(declarate_base(base)):
        user_id = Column(Integer, ForeignKey('user.id'))
        yt_user_id = Column(String(45))
        username_url = Column(String(100))
        motto = Column(Text())

        user = relationship('User')

    return Influencer
