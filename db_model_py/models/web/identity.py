from sqlalchemy import Column, String
from ..common.identity import declarate as declarate_base

def declarate(base):
    class Identity(declarate_base(base)):
        name_zh = Column(String(45))

    return Identity
