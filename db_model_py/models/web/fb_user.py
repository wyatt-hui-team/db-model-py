from ..common.fb_user import declarate as declarate_base

def declarate(base):
    class FBUser(declarate_base(base)):
        pass

    return FBUser
