from ..common.ig_user import declarate as declarate_base

def declarate(base):
    class IGUser(declarate_base(base)):
        pass

    return IGUser
