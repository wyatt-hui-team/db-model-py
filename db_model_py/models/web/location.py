from sqlalchemy import Column, String
from ..common.location import declarate as declarate_base

def declarate(base):
    class Location(declarate_base(base)):
        name_zh = Column(String(45))

    return Location
