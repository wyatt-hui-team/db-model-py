from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class BusinessUser(base, Model):
        __tablename__ = 'business_user'

        id = Column(Integer, primary_key=True)
        email = Column(String(255))
        name = Column(String(45))
        website = Column(String(45))
        businessTypeId = Column(Integer, ForeignKey('business_type.id'))
        businessNatureId = Column(Integer, ForeignKey('business_nature.id'))
        locationId = Column(Integer, ForeignKey('location.id'))
        loggedAt = Column(Integer)
        about = Column(Text())
        proStartDate = Column(Integer)
        expiredDate = Column(Integer)
        freeTrialStartDate = Column(Integer)
        freeTrialEndDate = Column(Integer)
        campaignCount = Column(Integer)
        campaignExpiryDate = Column(Integer)
        profilePic = Column(String(500))
        applyBusinessPro = Column(Integer)
        cpFirstName = Column(String(255))
        cpLastName = Column(String(255))
        position = Column(String(255))
        address = Column(Text())
        phone = Column(String(25))
        stripe_customer_id = Column(String(100))

        business_type = relationship('BusinessType')
        business_nature = relationship('BusinessNature')
        location = relationship('Location')

    return BusinessUser
