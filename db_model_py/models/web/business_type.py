from sqlalchemy import Column, Integer, String
from ..model import Model

def declarate(base):
    class BusinessType(base, Model):
        __tablename__ = 'business_type'

        id = Column(Integer, primary_key=True)
        name = Column(String(45))
        name_zh = Column(String(45))

    return BusinessType
