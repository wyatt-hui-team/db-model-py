from sqlalchemy import Column, String
from ..common.language import declarate as declarate_base

def declarate(base):
    class Language(declarate_base(base)):
        name_zh = Column(String(45))

    return Language
