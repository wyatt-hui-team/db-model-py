from sqlalchemy import Column, Integer, String
from ..model import Model

def declarate(base):
    class CommonIdentity(base, Model):
        __tablename__ = 'identity'

        id = Column(Integer, primary_key=True)
        name = Column(String(45))
        shortKey = Column(String(45))

    return CommonIdentity
