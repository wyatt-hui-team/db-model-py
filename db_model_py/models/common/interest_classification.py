from sqlalchemy import Column, Integer
from ..model import Model

def declarate(base):
    class CommonInterestClassification(base, Model):
        __abstract__ = True

        ic_1 = Column(Integer)
        ic_2 = Column(Integer)
        ic_3 = Column(Integer)
        ic_4 = Column(Integer)
        ic_5 = Column(Integer)
        ic_6 = Column(Integer)
        ic_7 = Column(Integer)
        ic_8 = Column(Integer)
        ic_9 = Column(Integer)
        ic_10 = Column(Integer)
        ic_11 = Column(Integer)
        ic_12 = Column(Integer)
        ic_13 = Column(Integer)
        ic_14 = Column(Integer)
        ic_15 = Column(Integer)
        ic_16 = Column(Integer)
        ic_17 = Column(Integer)
        ic_18 = Column(Integer)
        ic_19 = Column(Integer)
        ic_20 = Column(Integer)
        ic_100 = Column(Integer)
        ic_101 = Column(Integer)
        ic_102 = Column(Integer)
        ic_103 = Column(Integer)
        ic_104 = Column(Integer)
        ic_105 = Column(Integer)
        ic_106 = Column(Integer)
        ic_107 = Column(Integer)
        ic_108 = Column(Integer)
        ic_109 = Column(Integer)
        ic_110 = Column(Integer)
        ic_111 = Column(Integer)
        ic_112 = Column(Integer)
        ic_200 = Column(Integer)
        ic_201 = Column(Integer)
        ic_202 = Column(Integer)
        ic_203 = Column(Integer)
        ic_204 = Column(Integer)
        ic_300 = Column(Integer)
        ic_301 = Column(Integer)
        ic_302 = Column(Integer)
        ic_303 = Column(Integer)
        ic_304 = Column(Integer)
        ic_305 = Column(Integer)
        ic_306 = Column(Integer)
        ic_307 = Column(Integer)
        ic_308 = Column(Integer)
        ic_309 = Column(Integer)
        ic_310 = Column(Integer)
        ic_400 = Column(Integer)
        ic_401 = Column(Integer)
        ic_402 = Column(Integer)
        ic_403 = Column(Integer)
        ic_500 = Column(Integer)
        ic_501 = Column(Integer)
        ic_502 = Column(Integer)
        ic_503 = Column(Integer)
        ic_504 = Column(Integer)
        ic_505 = Column(Integer)
        ic_600 = Column(Integer)
        ic_601 = Column(Integer)
        ic_602 = Column(Integer)
        ic_700 = Column(Integer)
        ic_701 = Column(Integer)
        ic_702 = Column(Integer)
        ic_703 = Column(Integer)
        ic_704 = Column(Integer)
        ic_705 = Column(Integer)
        ic_706 = Column(Integer)
        ic_707 = Column(Integer)
        ic_708 = Column(Integer)
        ic_709 = Column(Integer)
        ic_710 = Column(Integer)
        ic_711 = Column(Integer)
        ic_800 = Column(Integer)
        ic_801 = Column(Integer)
        ic_802 = Column(Integer)
        ic_803 = Column(Integer)
        ic_804 = Column(Integer)
        ic_805 = Column(Integer)
        ic_806 = Column(Integer)
        ic_900 = Column(Integer)
        ic_901 = Column(Integer)
        ic_1000 = Column(Integer)
        ic_1001 = Column(Integer)
        ic_1002 = Column(Integer)
        ic_1003 = Column(Integer)
        ic_1004 = Column(Integer)
        ic_1005 = Column(Integer)
        ic_1100 = Column(Integer)
        ic_1101 = Column(Integer)
        ic_1102 = Column(Integer)
        ic_1103 = Column(Integer)
        ic_1104 = Column(Integer)
        ic_1105 = Column(Integer)
        ic_1200 = Column(Integer)
        ic_1201 = Column(Integer)
        ic_1202 = Column(Integer)
        ic_1203 = Column(Integer)
        ic_1204 = Column(Integer)
        ic_1205 = Column(Integer)
        ic_1206 = Column(Integer)
        ic_1207 = Column(Integer)
        ic_1208 = Column(Integer)
        ic_1500 = Column(Integer)
        ic_1501 = Column(Integer)
        ic_1502 = Column(Integer)
        ic_1503 = Column(Integer)
        ic_1504 = Column(Integer)
        ic_1505 = Column(Integer)
        ic_1506 = Column(Integer)
        ic_1507 = Column(Integer)
        ic_1508 = Column(Integer)
        ic_1509 = Column(Integer)
        ic_1510 = Column(Integer)
        ic_1511 = Column(Integer)
        ic_1512 = Column(Integer)
        ic_1513 = Column(Integer)
        ic_1600 = Column(Integer)
        ic_1601 = Column(Integer)
        ic_1602 = Column(Integer)
        ic_1603 = Column(Integer)
        ic_1604 = Column(Integer)
        ic_1605 = Column(Integer)
        ic_1606 = Column(Integer)
        ic_1607 = Column(Integer)
        ic_1700 = Column(Integer)
        ic_1701 = Column(Integer)
        ic_1702 = Column(Integer)
        ic_1703 = Column(Integer)
        ic_1704 = Column(Integer)
        ic_10000 = Column(Integer)
        ic_10001 = Column(Integer)
        ic_10002 = Column(Integer)
        ic_10003 = Column(Integer)
        ic_10004 = Column(Integer)
        ic_10005 = Column(Integer)
        ic_10100 = Column(Integer)
        ic_10101 = Column(Integer)
        ic_10102 = Column(Integer)
        ic_10200 = Column(Integer)
        ic_10201 = Column(Integer)
        ic_10202 = Column(Integer)
        ic_10203 = Column(Integer)
        ic_10204 = Column(Integer)
        ic_10205 = Column(Integer)
        ic_10206 = Column(Integer)
        ic_10207 = Column(Integer)
        ic_10208 = Column(Integer)
        ic_10209 = Column(Integer)
        ic_10210 = Column(Integer)
        ic_10211 = Column(Integer)
        ic_10300 = Column(Integer)
        ic_10301 = Column(Integer)
        ic_10302 = Column(Integer)
        ic_10303 = Column(Integer)
        ic_10304 = Column(Integer)
        ic_10305 = Column(Integer)
        ic_10306 = Column(Integer)
        ic_10307 = Column(Integer)
        ic_10400 = Column(Integer)
        ic_10401 = Column(Integer)
        ic_10402 = Column(Integer)
        ic_10403 = Column(Integer)
        ic_10700 = Column(Integer)
        ic_10701 = Column(Integer)
        ic_10702 = Column(Integer)
        ic_10800 = Column(Integer)
        ic_10801 = Column(Integer)
        ic_10802 = Column(Integer)
        ic_10803 = Column(Integer)
        ic_10804 = Column(Integer)
        ic_10805 = Column(Integer)
        ic_11100 = Column(Integer)
        ic_11101 = Column(Integer)
        ic_20100 = Column(Integer)
        ic_20101 = Column(Integer)
        ic_20102 = Column(Integer)
        ic_20103 = Column(Integer)
        ic_20104 = Column(Integer)
        ic_20105 = Column(Integer)
        ic_20106 = Column(Integer)
        ic_20107 = Column(Integer)
        ic_20300 = Column(Integer)
        ic_20301 = Column(Integer)
        ic_20302 = Column(Integer)
        ic_20303 = Column(Integer)
        ic_20304 = Column(Integer)
        ic_20305 = Column(Integer)
        ic_20306 = Column(Integer)
        ic_20307 = Column(Integer)
        ic_20400 = Column(Integer)
        ic_20401 = Column(Integer)
        ic_20402 = Column(Integer)
        ic_20403 = Column(Integer)
        ic_20404 = Column(Integer)
        ic_90000 = Column(Integer)
        ic_90001 = Column(Integer)
        ic_90002 = Column(Integer)
        ic_90003 = Column(Integer)
        ic_90004 = Column(Integer)
        ic_90005 = Column(Integer)
        ic_90100 = Column(Integer)
        ic_90101 = Column(Integer)
        ic_90102 = Column(Integer)
        ic_100100 = Column(Integer)
        ic_170000 = Column(Integer)
        ic_170001 = Column(Integer)
        ic_170002 = Column(Integer)
        ic_170003 = Column(Integer)
        ic_170004 = Column(Integer)
        ic_170100 = Column(Integer)
        ic_170101 = Column(Integer)
        ic_170102 = Column(Integer)
        ic_170103 = Column(Integer)
        ic_170104 = Column(Integer)
        ic_170105 = Column(Integer)
        ic_170106 = Column(Integer)
        ic_170200 = Column(Integer)
        ic_170201 = Column(Integer)
        ic_170202 = Column(Integer)
        ic_170400 = Column(Integer)
        ic_170401 = Column(Integer)


    return CommonInterestClassification
