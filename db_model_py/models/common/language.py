from sqlalchemy import Column, Integer, String
from ..model import Model

def declarate(base):
    class CommonLanguage(base, Model):
        __tablename__ = 'language'

        id = Column(Integer, primary_key=True)
        name = Column(String(45))

    return CommonLanguage
