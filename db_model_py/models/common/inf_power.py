from sqlalchemy import Column, Date, Float, Integer, String
from ..model import Model

def declarate(base):
    class CommonInfPower(base, Model):
        __abstract__ = True

        recordId = Column(String(16))
        loggedDate = Column(Date)
        recordDate = Column(Date)
        lastPostedSince = Column(Integer)
        explosive_post_age = Column(Integer)
        tier = Column(Integer)
        infPower = Column(Float)
        infPower_new = Column(Float)
        infPower_change = Column(Float)
        effective_explosiveness = Column(Float)
        reach_raw = Column(Float)
        appeal_new = Column(Float)
        explosiveness_new = Column(Float)
        engagement_new = Column(Float)
        interaction_new = Column(Float)
        pct_appeal = Column(Float)
        pct_engagement = Column(Float)
        pct_explosiveness = Column(Float)
        pct_reach = Column(Float)
        reach_scaled = Column(Float)
        engagement_scaled = Column(Float)
        explosiveness_scaled = Column(Float)
        appeal_scaled = Column(Float)
        final_scaled = Column(Float)
        flag_inactive = Column(Integer)

    return CommonInfPower
