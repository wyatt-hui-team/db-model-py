from sqlalchemy import Column, Integer, SmallInteger, String
from ..model import Model

def declarate(base):
    class CommonLocation(base, Model):
        __tablename__ = 'location'

        id = Column(Integer, primary_key=True)
        name = Column(String(45))
        key = Column(String(10))
        active = Column(SmallInteger)

    return CommonLocation
