from sqlalchemy import BigInteger, Column, Float, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class CommonFBPost(base, Model):
        __tablename__ = 'fb_post'

        id = Column(BigInteger, primary_key=True)
        fb_user_id = Column(BigInteger, ForeignKey('fb_user.id'))
        fb_post_id = Column(String(40))
        postType = Column(String(45))
        content = Column(Text)
        postDate = Column(Integer)
        tagCount = Column(Integer)
        likeCount = Column(Integer)
        shareCount = Column(Integer)
        commentCount = Column(Integer)
        pictureUrl = Column(String(255))
        interaction = Column(Float)
        link = Column(String(1000))
        updatedAt = Column(BigInteger)
        createdAt = Column(BigInteger)

        fb_user = relationship('FBUser')

    return CommonFBPost
