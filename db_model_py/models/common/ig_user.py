from sqlalchemy import BigInteger, Column, Float, Integer, SmallInteger, String, Text
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class CommonIGUser(base, Model):
        __tablename__ = 'ig_user'

        id = Column(Integer, primary_key=True)
        igId = Column(BigInteger)
        username = Column(String(45))
        name = Column(String(255))
        bio = Column(Text())
        profilePic = Column(String(255))
        postCount = Column(Integer)
        followerCount = Column(Integer)
        followingCount = Column(Integer)
        likeCount = Column(Integer)
        commentCount = Column(Integer)
        tagCount = Column(Integer)
        userInPhotoCount = Column(Integer)
        mentionCount = Column(Integer)
        firstPostDate = Column(Integer)
        lastPostDate = Column(Integer)
        recentPostCount = Column(Integer)
        recentCommentCount = Column(Integer)
        recentLikeCount = Column(Integer)
        updatedAt = Column(BigInteger)
        createdAt = Column(BigInteger)
        isClaimed = Column(SmallInteger)
        rating = Column(Float)
        followerPercentage = Column(Float)
        interactionPercentage = Column(Float)
        interaction = Column(Integer)
        weeklyScore = Column(Float)
        weeklyLoggedAt = Column(Integer)
        weeklyFollowerCount = Column(Integer)
        weeklyInteraction = Column(Integer)
        activeness = Column(Float)
        reach = Column(Float)
        explosiveness = Column(Float)
        appeal = Column(Float)
        newInteraction = Column(Float)
        engagement = Column(Float)
        activenessScore = Column(Integer)
        engagementScore = Column(Integer)
        reachScore = Column(Integer)
        explosivenessScore = Column(Integer)
        appealScore = Column(Integer)
        interactionScore = Column(Integer)
        followerStartedAt = Column(Integer)
        explosivenessPostScore = Column(Float)
        engagementRate = Column(Float)
        infPower = Column(Float)
        postListUpdatedAt = Column(Integer)

        influencer = relationship('Influencer')

    return CommonIGUser
