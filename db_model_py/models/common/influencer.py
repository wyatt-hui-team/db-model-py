from sqlalchemy import BigInteger, Column, Float, ForeignKey,\
    Integer, SmallInteger, String, Text
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class CommonInfluencer(base, Model):
        __tablename__ = 'influencer'

        id = Column(Integer, primary_key=True)
        profilePic = Column(String(255))
        name = Column(String(255))
        content = Column(Text())
        gender = Column(String(5))
        facebook = Column(String(255))
        fbId = Column(BigInteger)
        instagram = Column(String(255))
        youtube = Column(String(255))
        twitter = Column(String(255))
        phone = Column(String(20))
        email = Column(String(255))
        website = Column(String(500))
        snapchat = Column(String(45))
        weibo = Column(String(45))
        linkedin = Column(String(45))
        contactPerson = Column(String(45))
        isGroup = Column(SmallInteger)
        isAgency = Column(SmallInteger)
        identityId = Column(Integer, ForeignKey('identity.id'))
        locationId = Column(Integer, ForeignKey('location.id'))
        ig_user_id = Column(Integer, ForeignKey('ig_user.id'))
        fb_user_id = Column(Integer, ForeignKey('fb_user.id'))
        createdAt = Column(Integer)
        updatedAt = Column(Integer)
        verified = Column(SmallInteger)
        emailFromAbout = Column(String(255))
        infPower = Column(Float)
        ethnicity = Column(String(100))
        language_id = Column(Integer, ForeignKey('language.id'))
        edited = Column(SmallInteger)
        deleted_at = Column(SmallInteger)

        identity = relationship('Identity')
        location = relationship('Location')
        ig_user = relationship('IGUser')
        fb_user = relationship('FBUser')
        language = relationship('Language')

    return CommonInfluencer
