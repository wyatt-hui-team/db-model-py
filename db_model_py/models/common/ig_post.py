from sqlalchemy import BigInteger, Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class CommonIGPost(base, Model):
        __tablename__ = 'ig_post'

        ig_user_id = Column(BigInteger, ForeignKey('ig_user.id'))
        ig_post_id = Column(String(30), primary_key=True)
        content = Column(Text)
        postDate = Column(Integer)
        tagCount = Column(Integer)
        likeCount = Column(Integer)
        commentCount = Column(Integer)
        userInPhotoCount = Column(Integer)
        mentionCount = Column(Integer)
        pictureUrl = Column(String(255))
        videoUrl = Column(String(255))
        link = Column(String(255))
        updatedAt = Column(BigInteger)
        createdAt = Column(BigInteger)
        accessibility_caption = Column(Text)
        postType = Column(String(50))

        ig_user = relationship('IGUser')

    return CommonIGPost
