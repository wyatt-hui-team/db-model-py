from sqlalchemy import BigInteger, Column, Float, Integer, String, Text
from sqlalchemy.orm import relationship
from ..model import Model

def declarate(base):
    class CommonFBUser(base, Model):
        __tablename__ = 'fb_user'

        id = Column(Integer, primary_key=True)
        fbId = Column(BigInteger)
        coverPic = Column(String(255))
        profilePic = Column(String(255))
        name = Column(String(255))
        username = Column(String(255))
        category = Column(String(255))
        about = Column(Text())
        email = Column(String(255))
        website = Column(String(255))
        birthday = Column(String(45))
        phone = Column(String(45))
        bio = Column(Text())
        fanCount = Column(Integer)
        link = Column(String(255))
        createdAt = Column(Integer)
        updatedAt = Column(Integer)
        activeness = Column(Float)
        reach = Column(Float)
        explosiveness = Column(Float)
        appeal = Column(Float)
        interaction = Column(Float)
        activenessScore = Column(Integer)
        reachScore = Column(Integer)
        explosivenessScore = Column(Integer)
        engagementScore = Column(Integer)
        appealScore = Column(Integer)
        interactionScore = Column(Integer)
        followerStartedAt = Column(Integer)
        explosivenessPostScore = Column(Float)
        infPower = Column(Float)
        postListUpdatedAt = Column(Integer)
        followerPercentage = Column(Float)
        interactionPercentage = Column(Float)
        oldInteraction = Column(Integer)
        weeklyScore = Column(Float)
        weeklyLoggedAt = Column(Integer)
        weeklyFollowerCount = Column(Integer)
        weeklyInteraction = Column(Integer)
        engagementRate = Column(Float)

        influencer = relationship('Influencer')

    return CommonFBUser
