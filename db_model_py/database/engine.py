from sqlalchemy import create_engine as base_create_engine

def create_engine(opts=None):
    if opts is None:
        opts = {}

    url = opts.get('connection', 'mysql+mysqldb') + '://' +\
        opts.get('username', 'root') + ':' +\
        opts.get('password', '') + '@' +\
        opts.get('host', 'localhost') + ':' +\
        opts.get('port', '3306') + '/' +\
        opts.get('database', 'database')

    param_string = ''
    param = opts.get('param', {})
    for key, value in param.items():
        param_string += key + '=' + value
    if param_string:
        param_string = '?' + param_string

    return base_create_engine(url + param_string)
