def list_of_models_to_dicts(models_list):
    return [models_to_dicts(models) for models in models_list]

def models_to_dicts(models):
    return [model.to_dict() for model in models]

def convert_latin1_to_utf8(data, data_type, decode_errors='ignore'):
    if data_type == 'DATAFRAME_COLUMN':
        data = data.str.encode('latin-1').str.decode('utf-8', decode_errors)
    return data
