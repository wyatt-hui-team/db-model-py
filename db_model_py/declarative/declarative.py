from sqlalchemy.ext.declarative import declarative_base
from db_model_py.database.engine import create_engine

class Declarative():
    Base = None
    FBPost = None
    FBPostInterestClassification = None
    FBUser = None
    IGPost = None
    IGPostInterestClassification = None
    IGUser = None
    Influencer = None
    Location = None

    @staticmethod
    def create_declarative_base():
        return declarative_base()

    @classmethod
    def set_engine_to_base(cls, opts):
        cls.engine = engine = create_engine(opts)
        cls.Base.metadata.create_all(engine)

    @classmethod
    def close(cls):
        cls.Base.metadata.clear()
        cls.engine.dispose()
