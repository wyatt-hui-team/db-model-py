from db_model_py.models.crawl import fb_inf_power, fb_post, fb_post_interest_classification, \
    fb_user, fb_user_interest_classification, identity, \
    ig_inf_power, ig_post, ig_post_interest_classification, \
    ig_user, ig_user_interest_classification, \
    influencer, language, location
from .declarative import Declarative

class CrawlDeclarative(Declarative):
    Base = Declarative.create_declarative_base()

    FBInfPower = fb_inf_power.declarate(Base)
    FBPost = fb_post.declarate(Base)
    FBPostInterestClassification = fb_post_interest_classification.declarate(Base)
    FBUser = fb_user.declarate(Base)
    FBUserInterestClassification = fb_user_interest_classification.declarate(Base)
    Identity = identity.declarate(Base)
    IGInfPower = ig_inf_power.declarate(Base)
    IGPost = ig_post.declarate(Base)
    IGPostInterestClassification = ig_post_interest_classification.declarate(Base)
    IGUser = ig_user.declarate(Base)
    IGUserInterestClassification = ig_user_interest_classification.declarate(Base)
    Influencer = influencer.declarate(Base)
    Language = language.declarate(Base)
    Location = location.declarate(Base)
