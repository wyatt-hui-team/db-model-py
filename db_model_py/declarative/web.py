from db_model_py.models.web import business_nature, business_type, business_user,\
    fb_user, identity, ig_user, influencer, language, location, user
from .declarative import Declarative

class WebDeclarative(Declarative):
    Base = Declarative.create_declarative_base()

    BusinessNature = business_nature.declarate(Base)
    BusinessType = business_type.declarate(Base)
    BusinessUser = business_user.declarate(Base)
    FBUser = fb_user.declarate(Base)
    Identity = identity.declarate(Base)
    IGUser = ig_user.declarate(Base)
    Influencer = influencer.declarate(Base)
    Language = language.declarate(Base)
    Location = location.declarate(Base)
    User = user.declarate(Base)
