from db_model_py.models.automation import interest_classification,\
    test_followed_by, test_follower, test_influencer
from .declarative import Declarative

class AutomationDeclarative(Declarative):
    Base = Declarative.create_declarative_base()

    InterestClassification = interest_classification.declarate(Base)
    TestFollowedBy = test_followed_by.declarate(Base)
    TestFollower = test_follower.declarate(Base)
    TestInfluencer = test_influencer.declarate(Base)
