from hashlib import sha256
from sqlalchemy.exc import InvalidRequestError
from sqlalchemy.orm import sessionmaker

from db_model_py.declarative.declarative import Declarative
from db_model_py.models.model import Model

class Repository():
    Declarative = Declarative
    model = Model

    @classmethod
    def create_session(cls):
        return sessionmaker(bind=cls.Declarative.engine)()

    @classmethod
    def general_select_syntax(cls, builder, model, opts):
        builder = cls.query_wheres(builder, opts.get('wheres'), model)
        builder = cls.query_order_bys(builder, opts.get('order_bys'), model)
        builder = cls.query_limit(builder, opts.get('limit'))
        return builder

    @staticmethod
    def query_wheres(builder, wheres, default_model):
        if isinstance(wheres, list):
            for where in wheres:
                model = where.get('model', default_model)
                column = getattr(model, where.get('column', 'id'))
                value = where.get('value', '')
                operator = where.get('operator', 'equal')
                if operator == 'equal':
                    builder = builder.filter(column == value)
                elif operator == 'greater_than':
                    builder = builder.filter(column > value)
                elif operator == 'less_than':
                    builder = builder.filter(column < value)
                elif operator == 'greater_than_or_equal':
                    builder = builder.filter(column >= value)
                elif operator == 'less_than_or_equal':
                    builder = builder.filter(column <= value)
                elif operator == 'not_equal':
                    builder = builder.filter(column != value)
                elif operator == 'like':
                    builder = builder.filter(column.like(value))
                elif operator == 'in':
                    builder = builder.filter(column.in_(value))
                elif operator == 'not_in':
                    builder = builder.filter(~column.in_(value))
        return builder

    @staticmethod
    def query_order_bys(builder, order_bys, model):
        if isinstance(order_bys, list):
            for order_by in order_bys:
                column = getattr(model, order_by.get('column'))
                if order_by.get('direction') == 'desc':
                    column = column.desc()
                builder = builder.order_by(column)
        return builder

    @staticmethod
    def query_limit(builder, limit):
        if limit is not None:
            builder = builder.limit(limit)
        return builder

    @staticmethod
    def commit_session(session, close_session=True):
        try:
            session.commit()
        except InvalidRequestError as exception:
            print(exception)
        finally:
            if close_session:
                Repository.close_session(session)

    @staticmethod
    def close_session(session):
        return session.close()

    @staticmethod
    def sha256_hexdigest(text):
        return sha256(text.encode('utf-8')).hexdigest()
