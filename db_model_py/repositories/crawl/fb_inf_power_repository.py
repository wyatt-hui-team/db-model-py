from ..common.inf_power_repository import InfPowerRepository
from .repository import Repository

class FBInfPowerRepository(InfPowerRepository, Repository):
    model = Repository.Declarative.FBInfPower
