from ..common.inf_power_repository import InfPowerRepository
from .repository import Repository

class IGInfPowerRepository(InfPowerRepository, Repository):
    model = Repository.Declarative.IGInfPower
