from ..common.fb_post_repository import FBPostRepository as FBPostRepositoryBase
from .repository import Repository

class FBPostRepository(FBPostRepositoryBase, Repository):
    pass
