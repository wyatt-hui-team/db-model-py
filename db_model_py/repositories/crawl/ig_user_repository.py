from ..common.ig_user_repository import IGUserRepository as IGUserRepositoryBase
from .repository import Repository

class IGUserRepository(IGUserRepositoryBase, Repository):
    pass
