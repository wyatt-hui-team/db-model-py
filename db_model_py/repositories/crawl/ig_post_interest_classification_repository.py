from ..common.post_interest_classification_repository import PostICRepository
from .repository import Repository

class IGPostInterestClassificationRepository(PostICRepository, Repository):
    model = Repository.Declarative.IGPostInterestClassification
