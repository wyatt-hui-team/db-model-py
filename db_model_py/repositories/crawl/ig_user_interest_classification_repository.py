from ..common.user_interest_classification_repository import UserICRepository
from .repository import Repository

class IGUserInterestClassificationRepository(UserICRepository, Repository):
    model = Repository.Declarative.IGUserInterestClassification
