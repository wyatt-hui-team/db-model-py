from ..common.post_interest_classification_repository import PostICRepository
from .repository import Repository

class FBPostInterestClassificationRepository(PostICRepository, Repository):
    model = Repository.Declarative.FBPostInterestClassification
