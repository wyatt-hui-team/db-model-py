from ..common.ig_post_repository import IGPostRepository as IGPostRepositoryBase
from .repository import Repository

class IGPostRepository(IGPostRepositoryBase, Repository):
    pass
