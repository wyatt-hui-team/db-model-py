from db_model_py.repositories.repository import Repository as RepositoryBase
from db_model_py.declarative.crawl import CrawlDeclarative

class Repository(RepositoryBase):
    Declarative = CrawlDeclarative
