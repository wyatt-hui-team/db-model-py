from ..common.user_interest_classification_repository import UserICRepository
from .repository import Repository

class FBUserInterestClassificationRepository(UserICRepository, Repository):
    model = Repository.Declarative.FBUserInterestClassification
