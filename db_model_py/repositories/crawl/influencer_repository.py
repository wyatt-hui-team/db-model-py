from ..common.influencer_repository import InfluencerRepository as InfluencerRepositoryBase
from .repository import Repository

class InfluencerRepository(InfluencerRepositoryBase, Repository):
    pass
