from ..common.fb_user_repository import FBUserRepository as FBUserRepositoryBase
from .repository import Repository

class FBUserRepository(FBUserRepositoryBase, Repository):
    pass
