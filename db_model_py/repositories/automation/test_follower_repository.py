from .repository import Repository

class TestFollowerRepository(Repository):
    @classmethod
    def merge_test_followers(cls, followers, influencer_id):
        session = cls.create_session()
        influencer_id = cls.sha256_hexdigest(influencer_id)
        for d_follower in followers:
            user_id = cls.sha256_hexdigest(d_follower.get('username'))
            follower = cls.Declarative.TestFollower(**d_follower)
            follower.user_id = user_id
            session.merge(follower)

            followed_by = cls.Declarative.TestFollowedBy(
                influencer_id=influencer_id,
                follower_id=user_id
            )
            session.merge(followed_by)
        return cls.commit_session(session)
