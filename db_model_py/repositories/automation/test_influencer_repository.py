from sqlalchemy.orm import joinedload
from .repository import Repository

class TestInfluencerRepository(Repository):
    @classmethod
    def merge_test_influencer(cls, d_influencer):
        session = cls.create_session()
        influencer = cls.Declarative.TestInfluencer(**d_influencer)
        influencer.user_id = cls.sha256_hexdigest(d_influencer.get('username'))
        session.merge(influencer)
        return cls.commit_session(session)

    @classmethod
    def list_test_influencer(cls):
        session = cls.create_session()
        result = session.query(cls.Declarative.TestInfluencer).all()
        cls.close_session(session)
        return result

    @classmethod
    def get_test_influencer_by_username(cls, username, opts=None):
        if opts is None:
            opts = {}

        session = cls.create_session()

        influencer_model = cls.Declarative.TestInfluencer
        query = session.query(influencer_model).\
            filter(influencer_model.username == username)

        aggregates = opts.get('aggregates', {})
        if 'followers' in aggregates:
            query = query.options(joinedload(influencer_model.followers))

        result = query.one()

        cls.close_session(session)
        return result
