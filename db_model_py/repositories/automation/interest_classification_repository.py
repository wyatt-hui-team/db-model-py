from .repository import Repository

class InterestClassificationRepository(Repository):
    @classmethod
    def list_interest_classification(cls, opts=None):
        if opts is None:
            opts = {}

        ic_model = cls.Declarative.InterestClassification
        session = cls.create_session()
        builder = session.query(ic_model)

        builder = cls.general_select_syntax(builder, ic_model, opts)

        result = builder.all()

        cls.close_session(session)
        return result
