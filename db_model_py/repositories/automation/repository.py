from db_model_py.repositories.repository import Repository as RepositoryBase
from db_model_py.declarative.automation import AutomationDeclarative

class Repository(RepositoryBase):
    Declarative = AutomationDeclarative
