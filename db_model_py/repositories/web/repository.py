from db_model_py.repositories.common.repository import Repository as RepositoryBase
from db_model_py.declarative.web import WebDeclarative

class Repository(RepositoryBase):
    Declarative = WebDeclarative
