from .repository import Repository as RepositoryBase
from ..common.influencer_repository import InfluencerRepository as InfluencerRepositoryBase

class InfluencerRepository(RepositoryBase, InfluencerRepositoryBase):
    pass
