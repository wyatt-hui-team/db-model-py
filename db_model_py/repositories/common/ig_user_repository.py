from ..repository import Repository

class IGUserRepository(Repository):
    @classmethod
    def list_ig_user(cls, opts=None):
        if opts is None:
            opts = {}

        declarative = cls.Declarative
        ig_user_model = declarative.IGUser
        session = cls.create_session()

        builder = session.query(ig_user_model)

        aggregates = opts.get('aggregates', [])
        if 'influencer' in aggregates:
            influencer_model = declarative.Influencer
            builder = builder.add_entity(influencer_model).\
                join(influencer_model, ig_user_model.id == influencer_model.ig_user_id)

        builder = cls.general_select_syntax(builder, ig_user_model, opts)

        result = builder.all()

        cls.close_session(session)
        return result
