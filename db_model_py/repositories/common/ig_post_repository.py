from ..repository import Repository

class IGPostRepository(Repository):
    @classmethod
    def list_ig_post(cls, opts=None):
        if opts is None:
            opts = {}

        declarative = cls.Declarative
        ig_post_model = declarative.IGPost
        session = cls.create_session()

        builder = session.query(ig_post_model)

        aggregates = opts.get('aggregates', [])
        if 'ig_user' in aggregates:
            ig_user_model = declarative.IGUser
            builder = builder.add_entity(ig_user_model).\
                join(ig_user_model, ig_post_model.ig_user_id == ig_user_model.id)
            if 'influencer' in aggregates:
                influencer_model = declarative.Influencer
                builder = builder.add_entity(influencer_model).\
                    join(influencer_model, ig_user_model.id == influencer_model.ig_user_id)
        if 'ig_post_ic' in aggregates:
            ig_post_ic_model = declarative.IGPostInterestClassification
            builder = builder.add_entity(ig_post_ic_model).\
                join(ig_post_ic_model, ig_post_model.ig_post_id == ig_post_ic_model.ig_post_id)

        builder = cls.general_select_syntax(builder, ig_post_model, opts)

        result = builder.all()

        cls.close_session(session)
        return result
