from ..repository import Repository

class FBUserRepository(Repository):
    @classmethod
    def list_fb_user(cls, opts=None):
        if opts is None:
            opts = {}

        declarative = cls.Declarative
        fb_user_model = declarative.FBUser
        session = cls.create_session()

        builder = session.query(fb_user_model)

        aggregates = opts.get('aggregates', [])
        if 'influencer' in aggregates:
            influencer_model = declarative.Influencer
            builder = builder.add_entity(influencer_model).\
                join(influencer_model, fb_user_model.id == influencer_model.fb_user_id)

        builder = cls.general_select_syntax(builder, fb_user_model, opts)

        result = builder.all()

        cls.close_session(session)
        return result
