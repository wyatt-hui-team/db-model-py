from ..repository import Repository

class PostICRepository(Repository):
    @classmethod
    def merge_post_ics(cls, post_ic_dicts):
        model = cls.model
        session = cls.create_session()

        for post_ic_dict in post_ic_dicts:
            post_ic = model(**post_ic_dict)
            session.merge(post_ic)

        return cls.commit_session(session)
