from ..repository import Repository as RepositoryBase

class InfPowerRepository(RepositoryBase):
    @classmethod
    def merge_inf_powers(cls, inf_power_dicts):
        model = cls.model
        session = cls.create_session()

        for inf_power_dict in inf_power_dicts:
            inf_power = model(**inf_power_dict)
            session.merge(inf_power)

        return cls.commit_session(session)
