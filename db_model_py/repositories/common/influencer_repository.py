from ..repository import Repository

class InfluencerRepository(Repository):
    @classmethod
    def list_influencer(cls, opts=None):
        if opts is None:
            opts = {}

        declarative = cls.Declarative
        influencer_model = declarative.Influencer
        session = cls.create_session()

        builder = session.query(influencer_model)

        aggregates = opts.get('aggregates', [])
        if 'location' in aggregates:
            location_model = declarative.Location
            builder = builder.add_entity(location_model).\
                join(location_model, influencer_model.locationId == location_model.id)

        builder = cls.general_select_syntax(builder, influencer_model, opts)

        result = builder.all()

        cls.close_session(session)
        return result

    @classmethod
    def get_influencer(cls, influencer_id):
        influencer_model = cls.Declarative.Influencer
        session = cls.create_session()
        result = session.query(influencer_model) \
            .filter(influencer_model.id == influencer_id) \
            .one()

        cls.close_session(session)
        return result
