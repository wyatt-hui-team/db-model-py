from ..repository import Repository

class UserICRepository(Repository):
    @classmethod
    def merge_user_ics(cls, user_ic_dicts):
        model = cls.model
        session = cls.create_session()

        for user_ic_dict in user_ic_dicts:
            user_ic = model(**user_ic_dict)
            session.merge(user_ic)

        return cls.commit_session(session)
