from ..repository import Repository

class FBPostRepository(Repository):
    @classmethod
    def list_fb_post(cls, opts=None):
        if opts is None:
            opts = {}

        declarative = cls.Declarative
        fb_post_model = declarative.FBPost
        session = cls.create_session()

        builder = session.query(fb_post_model)

        aggregates = opts.get('aggregates', [])
        if 'fb_user' in aggregates:
            fb_user_model = declarative.FBUser
            builder = builder.add_entity(fb_user_model).\
                join(fb_user_model, fb_post_model.fb_user_id == fb_user_model.id)
            if 'influencer' in aggregates:
                influencer_model = declarative.Influencer
                builder = builder.add_entity(influencer_model).\
                    join(influencer_model, fb_user_model.id == influencer_model.fb_user_id)
        if 'fb_post_ic' in aggregates:
            fb_post_ic_model = declarative.FBPostInterestClassification
            builder = builder.add_entity(fb_post_ic_model).\
                join(fb_post_ic_model, fb_post_model.fb_post_id == fb_post_ic_model.fb_post_id)

        builder = cls.general_select_syntax(builder, fb_post_model, opts)

        result = builder.all()

        cls.close_session(session)
        return result
